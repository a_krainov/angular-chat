import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatProgressBarModule,
    MatToolbarModule,
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {routes} from './app-routing.module';
import {ReactiveFormsModule} from "@angular/forms";
import {ApiService} from "./api.service";
import {HttpClientModule} from "@angular/common/http";
import { RoomComponent } from './room/room.component';


@NgModule({
    declarations: [
        AppComponent,
        HomePageComponent,
        RoomComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,

        MatToolbarModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatDividerModule,
        MatProgressBarModule,
        MatCardModule,
        ReactiveFormsModule,

        RouterModule.forRoot(routes),
    ],
    providers: [
        ApiService
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}