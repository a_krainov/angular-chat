import { Component } from '@angular/core';
import { WebsocketService } from './websocket.service';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
    providers: [ WebsocketService, ApiService ]
})
export class AppComponent {
  title = 'chat';
  userName:string;

    constructor() {
        this.userName = localStorage.getItem('name');
    }

}
