import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../api.service";

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

    form: FormGroup;
    author: String;
    room:String;
    message:String;
    time:String;

    messageArray: Array<{ author: String, message: String, time: String , room: String }> = [];

    constructor(
        private formBuilder: FormBuilder,
        private apiService: ApiService,
    ) {
        this.apiService.messages.subscribe(msg => {
            this.messageArray.push(msg);
        });

    }

  ngOnInit() {
      this.form = this.formBuilder.group({
          text: '',
      });
      this.author = localStorage.getItem('name');
  }
    sendMessage() {

        if (this.author != '') {

            //this.author = localStorage.getItem('name');
            this.message = this.form.get('text').value;
            this.time = new Date().toString();
            this.room = 'all';

            this.form.get('text').reset();

            this.apiService.sendMessage({author:this.author, message:this.message, time:this.time, room:this.room});
            this.messageArray.push({author:this.author, message:this.message, time:this.time, room:this.room});

        //}

    }
}
