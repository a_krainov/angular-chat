import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { WebsocketService } from './websocket.service';

const CHAT_URL = 'wss://do.brainfaq.ru/chat?token=g6vvucKrCUZ3PTvL0F8R6cjdFi0qGyEgrdvEzVgFsRyBE1FEticHiGHTgWVJetq3';

export interface Message {
    author: string,
    message: string
    time: string
    room: string
}

@Injectable()
export class ApiService {
    public messages: Subject<Message>;

    constructor(wsService: WebsocketService) {
        this.messages = <Subject<Message>>wsService
            .connect(CHAT_URL)
            .map((response: MessageEvent): Message => {
                let data = JSON.parse(response.data);
                return {
                    author: data.author,
                    message: data.message,
                    time: data.time,
                    room: data.room,
                }
            });
    }

    sendMessage(data)
    {
        this.messages.next(data);
        console.log(this.messages.valueOf());

        //this.messages.asObservable()

    }

/*    newMessageReceived(){
        let observable = new Observable<{user:String, message:String}>(observer=>{
            this.messages.on('new message', (data)=>{
                observer.next(data);
            });
            return () => {this.messages.disconnect();}
        });

        return observable;
    }*/
}