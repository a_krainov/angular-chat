import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../api.service";
import {Router} from '@angular/router';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {

    form: FormGroup;
    author: String;
    room:String;
    message:String;
    time:String;

    messageArray: Array<{ author: String, message: String, time: String , room: String }> = [];

    constructor(
        private formBuilder: FormBuilder,
        private apiService: ApiService,
        private router: Router,
    ) {
        this.apiService.messages.subscribe(msg => {
            this.messageArray.push(msg);
        });

    }


    ngOnInit() {
        this.form = this.formBuilder.group({
            name: '',
            text: '',
        });

    }

    name = new FormControl('', [Validators.required, Validators.nullValidator]);

    getErrorMessage() {
        return this.name.hasError('required') ? 'Вы должны ввести значение' :
            this.name.hasError('name') ? 'Not a name' :
                '';
    }

    sendMessage() {

        if (this.form.get('name').value != '') {
            // Добавим Имя в Сессию
            localStorage.setItem('name', this.form.get('name').value);
            //Отправим на страницу /room
            this.router.navigate(['/room']);
            //this.form.get('name').disable();

/*
            this.author = this.form.get('name').value;
            this.message = this.form.get('text').value;
            this.time = new Date().toString();
            this.room = 'all';

            this.form.get('text').reset();

            this.apiService.sendMessage({author:this.author, message:this.message, time:this.time, room:this.room});
            this.messageArray.push({author:this.author, message:this.message, time:this.time, room:this.room});
*/

        }

    }

}