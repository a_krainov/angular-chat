import { Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { RoomComponent } from './room/room.component';

export const routes: Routes = [
    {path: '', component: HomePageComponent},
    {path: 'room', component: RoomComponent},
];